import { useState } from 'react'
import dice1 from "./images/Dice1.png"
import dice2 from "./images/Dice2.png"
import dice3 from "./images/Dice3.png"
import dice4 from "./images/Dice4.png"
import dice5 from "./images/Dice5.png"
import dice6 from "./images/Dice6.png"



function App() {



  let diceimages = [dice1, dice2, dice3, dice4, dice5, dice6]

  const [image1, setNewImage1] = useState(diceimages[0])
  const [image2, setNewImage2] = useState(diceimages[1])


  let RollDice = () => {
    var random1 = Math.floor(Math.random() * 6)
    var random2 = Math.floor(Math.random() * 6)
    if(random1==random2){
      document.getElementById('winner').textContent=`Tie`
    }
    else{
      random1>random2 ?document.getElementById('winner').textContent=`player1 wins!`:document.getElementById('winner').textContent=`player2 wins!`
    }
    
    setNewImage1(diceimages[random1])
    setNewImage2(diceimages[random2])
  }


  return (
    <>
      <div className='flex w-[50%] bg-black h-[50vh] justify-center items-center'>
        <p id="winner" className=' absolute -mt-80 inline-block text-red-400 text-center pb-5 text-2xl'></p>
        <div className='flex flex-row space-x-20 '>
          <div>
            <p className=' text-red-400 text-center pb-5 text-xl'>Player1</p>
            <img className='w-[200px] h-[20vh]' src={image1}></img>
          </div>
          <div>
            <p className=' text-red-400 text-center pb-5 text-xl'>Player2</p>
            <img className='w-[200px] h-[20vh]' src={image2}></img>
          </div>

        </div>
        <div className=''>
        <button className='absolute inline-block bg-red-500 px-5 py-2 mt-40 -ml-72 rounded-lg' onClick={RollDice}> Roll Dice</button>
        </div>
      </div>
    </>
  )
}

export default App
